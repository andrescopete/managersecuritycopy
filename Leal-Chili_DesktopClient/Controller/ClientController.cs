﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Diagnostics;
using System.IO;
using RestSharp;
using System.Reflection;
using System.Collections.Generic;
using System;
using ModelLayer;
using Output;
using System.Text;
using System.Linq;
using System.Globalization;
using System.Deployment.Application;
using System.Security.Cryptography;
using Controller.Properties; //Para guardar las variables del sistema
using System.Threading;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Controller
{
    public class ClientController
    {
        private string webServiceHost = "http://chiliawsho.ddns.net:5000";
        private ConnectionSQLAnywhere connection;
        private ConsultasDB consultasDB;
        private WatcherXML watcher;
        private Printer view;
        private Log messagesLog; //LogMessages
        private Log exceptionsLog; //LogExcepciones
        private List<string> InvoiceSenderList = new List<string>(); //Aqui estan los ids de factura filtrados por memcode
        private string tokenChili = "";
        private bool successLogin = false;
        private Thread websock;
        private Thread InternetConection;
        private Thread restartSocket;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public ClientWebSocket socket;
        public string messageWebsocket;
        public bool bandera;
        private bool initWebSockCli;
        private int ContDayBloq = 0;
        private int contadorSinInternet=0;
        static UTF8Encoding encoder = new UTF8Encoding();
        public ClientController(WatcherXML wat)
        {
            try
            {

                watcher = wat;
                InitializeLogs();
                InitializeOutput();
                InitializeSession();
                

                /*
                    * Lo siguiente se utilizaba cuando mostrabamos la consola
                    * 
                ---------------------------------

                //La siguiente instruccion pone a escuchar la entrada del teclado
                //Cuando se apreta ctrl-c, el programa termina
                Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelKeyPressed);

                ---------------------------------

                do
                {
                    messagesLog.SaveMessage("Para salir, presione Esc o las teclas ctrl-c");
                } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
                SystemExit();

                */

            }
            catch (Exception e)
            {
                exceptionsLog.SaveMessage(e.ToString());
            }
        }
        

        private void InitializeLogs()
        {
            messagesLog = (Log)LogMessages.GetLog();
            exceptionsLog = (Log)LogExcepciones.GetLog();

            
        }

        /*
         * Método para generar la conexion con la bd e inicializar el watcher e inicializa el saludo de la interfaz grafica.
         * 
         * */
        public void InitializeOutput()
        {
            try
            {
                view = new Printer();
                //Console.WriteLine("Esperando 1 segundo");
                System.Threading.Thread.Sleep(1000); //Para esperar que instancie El hilo de TrayNotif
                view.ShowBalloonNotification("Bienvenido \nIntegracion Chili-Leal");

                InternetConection = new Thread(new ThreadStart(validateConect));
                InternetConection.Start();
                Thread.Sleep(1500);
                

                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    Version myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                    //view.ShowInLogWindow(string.Concat("ClickOnce published Version: v", myVersion));
                    view.PrintInLogWindow(string.Concat("Current version: v", myVersion));
                }
            }
            catch (NullReferenceException nullEx)
            {
                messagesLog.SaveMessage(nullEx.ToString());
            }

            //Suscribir a la gui:
            IEventPrinter d = (IEventPrinter)view;
            d.OnReset += new EventHandler((object e, EventArgs a) => {
                view.PrintInLogWindow("Reseting from controller");
                InitializeSession();
            });
            d.OnLogin += new LoginEventHandler((string username, string password) => {
                //Guarda en las propiedades el usr y pwd para no seguirlo pidiendo #Deberiamos borrarlo cada cierto tiempo
                Settings.Default.Username = username;
                Settings.Default.Password = password;
                Settings.Default.Save();
                view.PrintInLogWindow("Guardados datos de inicio de sesión.");
                view.PrintLoginNotification("Guardados datos de inicio de sesión.");
                //Despues de cierto tiempo, borrar lo que imprime en el txtloginNotif
                Thread t = new Thread(new ThreadStart(RemoveMessageFromTxtLoginNotification));
                t.Start();
                InitializeSession();
            });



           //capturamos evento de cierre de la Gui 
             d.OnCloseForm+=new EventHandler((object sender, EventArgs e) =>{
                string LogoutUserObject = Settings.Default.Username.ToString();
                var jsonDataUser = new
                {
                    Username = LogoutUserObject,
                    TokenUser = tokenChili
                };

                string DataSendUser = JsonConvert.SerializeObject(jsonDataUser);
                var client = new RestClient("http://chiliawsho.ddns.net:5000/api/User/Logout");
                var request = new RestRequest(Method.POST);
                string LogoutUser = Settings.Default.Username.ToString();
                Console.WriteLine("Usuario setting:" + LogoutUser);
                Console.WriteLine("El json:" + DataSendUser);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", DataSendUser, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                
                Console.WriteLine("Respuesta:"+ response.Content.ToString());
                Thread.Sleep(2000);
                Console.WriteLine("Estoy saliendo de la Gui");
                Console.WriteLine("JsonFormat:" + DataSendUser);
                view.PrintInLogWindow("cerrando aplicativo.....");
                 // websocketCliente.CloseWebsocket().Wait();
                 if (initWebSockCli == true)
                 {
                     Thread final = new Thread(new ThreadStart(FinalWeb));
                     final.Start();
                     websock.Abort();
                     Thread.Sleep(1800);
                     final.Abort();
                     //Thread.Sleep(1500);
                     InternetConection.Abort();
                 }
                 else { };
             });

        }

        public void InicializarWeb()
        {
            CallWebSocketServer().Wait();
        }


        public void FinalWeb()
        {
            CloseWebsocket().Wait();
        }
        public async Task CallWebSocketServer()
        {
            try {

            socket = new ClientWebSocket();
            //socket.Options.SetRequestHeader("andres", "andres");
            socket.Options.KeepAliveInterval = TimeSpan.FromSeconds(20);
                //view = new Printer();
            string nameSucusal = consultasDB.GetNombreSucursal();
            Console.WriteLine("Name sucursal:" + nameSucusal);
            await socket.ConnectAsync(new Uri("ws://chiliawsho.ddns.net:5000/chat/api/prueba/websocket?Name="+nameSucusal), CancellationToken.None);
            byte[] buffer = encoder.GetBytes("massive");
               
            //messageWebsocket = receiveTask.ToString();
            //Console.WriteLine("MessageWebsocket:" + messageWebsocket);
            //Console.WriteLine("Valor de la Task WebSocket Abierto:" + receiveTask.Status);

            //    await socket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, true, CancellationToken.None);


            //await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "close", CancellationToken.None);
            Console.WriteLine("Abriendo Websocket...");
            initWebSockCli = true;
            Task receiveTask = Receive(socket);
            receiveTask.Wait();
            }
            catch(Exception e){
                initWebSockCli = false;
                Console.WriteLine("Problemas al crear el WebSocket:" + e);
                Console.WriteLine("Verifique que el usuario sea correcto"+ initWebSockCli);
                exceptionsLog.SaveMessage("No se pudo inciar websocket:" + e);
            }


        }


        public async Task CloseWebsocket()
        {
            try { 
            Console.WriteLine("Cerrando el socket");
            await socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "close", CancellationToken.None);
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine("Excepcion en el cierre:" + e);
            }
        }

        
        public async Task Receive(ClientWebSocket socket)
        {

            
            try
            {
                byte[] recvBuffer = new byte[64 * 1024];
                while (socket.State == WebSocketState.Open)
                { 
                    
                    var result = await socket.ReceiveAsync(new ArraySegment<byte>(recvBuffer), CancellationToken.None);
                    string ResponseSanitize = SanitizeResponse(Encoding.UTF8.GetString(recvBuffer, 0, result.Count).ToString());
                    JObject RespuestaApi = JObject.Parse(ResponseSanitize);
                    string tipo = RespuestaApi["tipo"].ToString();
                    string message = RespuestaApi["message"].ToString();
                    Console.WriteLine("Tipo:" + tipo);
                    if (tipo.Equals("manager"))
                    {
                        Console.WriteLine("El mensaje no pertence a este Socket");
                    }else if (tipo.Equals("especifico") && message.Equals("Bloquear"))
                    {
                        string path = @"C:\PIXELPOS";
                        try
                        {
                            if (Directory.Exists(path))
                            {
                                string pathArchivo = @"C:\PIXELPOS\LicenseManager.exe";

                                Console.WriteLine("Directorio Existe");

                                if (File.Exists(pathArchivo))
                                {
                                    Console.WriteLine("El archivo Existe");
                                    File.Delete(pathArchivo);
                                }
                                else
                                {
                                    Console.WriteLine("El archivo no Existe");
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Directorio no Existe:" + e);
                        }
                    }
                    else { 
                    Console.WriteLine("Client got {0} bytes", result.Count);
                    Console.WriteLine(Encoding.UTF8.GetString(recvBuffer, 0, result.Count));
                    if (result.Count > 0)
                    {
                        string messageUser = RespuestaApi["message"].ToString();    
                        bandera = true;
                        messageWebsocket = messageUser;
                        //MessageBox.Show("mensaje:" + messageWebsocket);

                    }
                    view.ShowBalloonNotification("Notificaciones:"+messageWebsocket);
                    Thread.Sleep(4550);
                    Console.WriteLine("Bandera:" + bandera + "messag:" + messageWebsocket);
                    if (result.MessageType == WebSocketMessageType.Close)
                    {
                        Console.WriteLine("Close loop complete");
                        break;
                    }
                }
            }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in receive - {0}", ex.Message);
            }
        }

        
        public void ValidateConnectionInternet()
        {
            bool bandera = GetconnectionInternet();
            while (GetconnectionInternet()==true)
            {
                
                Console.WriteLine("Con Conexion a internet");
                Thread.Sleep(1000);
                

            } while (GetconnectionInternet() == false) {

                
                Console.WriteLine("No hay conexion a internet");
                Thread.Sleep(1000);
                
                
            }

            
        }

        public void validateConect()
        {
        
         try { 
            int Desc;
            bool isconnect = false;
            contadorSinInternet = 0;
            while (true) { 
                while (InternetGetConnectedState(out Desc, 0)==true)
                {
                   Console.WriteLine(" VALOR CONEXION -------------->>>>>" + contadorSinInternet);
                        if (contadorSinInternet>0)
                        {
                            try
                            {
                                /*Iniciamos un hilo con una nueva conexion del websocket que tumba la anterior por interoperabilidad*/
                                restartSocket = new Thread(new ThreadStart(InicializarWeb));
                                restartSocket.Start();
                                Console.WriteLine("WEBSOCKET NUEVO DE CONEXION-------->>>><<<>()");
                                
                            }

                            catch (Exception e)
                            {
                                Console.WriteLine("No se pudo Conectar el WebSocket:" + e);
                                Console.WriteLine("Valide que el usuario sea correcto.");
                            }

                            
                            
                        }
                    contadorSinInternet =0;
                    isconnect = InternetGetConnectedState(out Desc, 0);
                    Console.WriteLine("Hay conexion a internet" + Desc);
                     Console.WriteLine("Contador sin Internet metodo con Internet:" + contadorSinInternet);
                    Thread.Sleep(6000);
                        
                    }

                while (InternetGetConnectedState(out Desc, 0)==false)
                {
                    DateTime fechaInicialManger;
                    DateTime fechaFinalCorte;
                    int contDay = 0;
                    contadorSinInternet++;
                    isconnect = InternetGetConnectedState(out Desc, 0);
                    Console.WriteLine(" No Hay conexion a internet" +Desc);
                        Console.WriteLine("Contador sin Internet:" + contadorSinInternet);
                    Thread.Sleep(6000);
                        /*Controlar intermitencia durante los ping realizados en el dia*/
                        if (contadorSinInternet == 2)
                        {
                            contDay++;
                            ContDayBloq = 0;
                            int contDaySetting = Controller.Properties.Settings.Default.ContDays;
                            ContDayBloq = contDay + contDaySetting;
                            Console.WriteLine("valor del contday:" + ContDayBloq);
                            Controller.Properties.Settings.Default.ContDays= ContDayBloq;
                            Controller.Properties.Settings.Default.Save();
                            Controller.Properties.Settings.Default.FechaInicio = DateTime.Today;
                            fechaInicialManger = Controller.Properties.Settings.Default.FechaInicio;
                            Controller.Properties.Settings.Default.FechaInicio = fechaInicialManger;
                            Controller.Properties.Settings.Default.FechaActual = DateTime.Today;
                            Console.WriteLine("CONTADOR DE FECHAS:"+ Controller.Properties.Settings.Default.FechaInicio+"-"+ Controller.Properties.Settings.Default.FechaActual);
                            Console.WriteLine("vamos a restar un dia" + Desc + "" + DateTime.Today+"Valor dias:"+ ContDayBloq);

                            if (ContDayBloq == 30)
                            {
                                Console.WriteLine("Entre al if");
                                string path = @"C:\PIXELPOS";
                                try
                                {
                                    if (Directory.Exists(path))
                                    {
                                        string pathArchivo = @"C:\PIXELPOS\LicenseManager.exe";

                                        Console.WriteLine("Directorio Existe");

                                        if (File.Exists(pathArchivo))
                                        {
                                            Console.WriteLine("El archivo Existe");
                                            File.Delete(pathArchivo);
                                            int reset = 0;
                                            Controller.Properties.Settings.Default.ContDays = reset;
                                            ContDayBloq = 0;
                                            Controller.Properties.Settings.Default.Save();
                                        }
                                        else
                                        {
                                            Console.WriteLine("El archivo no Existe");
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("Directorio no Existe:" + e);
                                }

                            }
                        }

                    }                
               }
              }

          catch (Exception e)
              {
                 Console.WriteLine("Excepcion validaConect:" + e);
             }
        }

        public void ValidatePingConnectionInternet()
        {
            
            while (SendPing() == true)
            {

                Console.WriteLine("Con Conexion a internet");
                Thread.Sleep(1000);

            } while (SendPing() == false)
            {

                Console.WriteLine("No hay conexion a internet");
                Thread.Sleep(1000);

            }
            
        }





        public bool SendPing()
        {
            try { 
            System.Net.NetworkInformation.Ping MyPing = new System.Net.NetworkInformation.Ping();
            System.Net.NetworkInformation.PingReply reply = MyPing.Send("www.google.com",1000);
            if (reply != null) 
            {
                Console.WriteLine("Status:" + reply.Status + "" + reply.Address);
                return true;
            }
            else
            {
                Console.WriteLine("Status:" + reply.Status);
                return false;
            }
            }
            catch (Exception e)
            {
                messagesLog.SaveMessage("Excepcion en el ping:" + e);
               

                return false;
            }
        }

        public bool GetconnectionInternet()
        {
            System.Net.IPHostEntry host=null;
            try { 
             host = System.Net.Dns.GetHostEntry("www.google.com.co");
                
                Console.WriteLine(host.HostName+""+host.AddressList);
            return true;
            }catch(Exception e)
            {
                
                messagesLog.SaveMessage("Excepción encontrada en la conexión:"+e);
                return false;
            }
        }


        /*public void FinalyWebSocket()
        {
            websocketCliente.CloseWebsocket().Wait();

        }

        public void InitializeWebSocketCli()
        {
            websocketCliente = new WebsocketConecction();
            websocketCliente.CallWebSocketServer().Wait();
            websocketCliente.Receive(websocketCliente.socket).Wait();
            view.ShowBalloonNotification(websocketCliente.message());


        }*/

        private void InitializeSession()
        {
            //Lo primero es hacer el login:
            // Hacemos el login del agente
            try
            {
                string username = Controller.Properties.Settings.Default.Username;
                string password = Controller.Properties.Settings.Default.Password;
                if (username.Equals("") || password.Equals(""))
                {
                    view.PrintInLogWindow("Dirijase a la ventana de configuración y realice el login");
                }
                else
                {
                    ChiliLogin(username, password);
                }
            }

             
            catch (Exception e)
            {
                view.PrintInLogWindow("Fallo al hacer login");
                messagesLog.SaveMessage("Agente Login fallo.");
                exceptionsLog.SaveMessage(e.ToString());
                view.PrintInLogWindow("El valor de casma es:");
            }

            if (successLogin == true)
            {
                //Aqui busca en el sistema el dsn y el path de xml 
                // si no esta manda a la vista a que pregunte por el path y el dsn y luego lo guarde en el sistema
                string xmlPath = "";
                string DSN = "";
                try
                {
                    xmlPath = view.getXMLPath();
                    DSN = view.getDSN();
                    messagesLog.SaveMessage("El dns ingresado:" + DSN);
                   // view.PrintInLogWindow("Variables del usuario: " + DSN + " " + xmlPath);
                }
                catch (Exception e) when (e is NullReferenceException nullE || e is InvalidOperationException)
                {
                    view.ShowSettingsTab();
                    exceptionsLog.SaveMessage(e.ToString());
                    view.PrintInLogWindow("Configure el DSN y el XmlPath");
                    return;
                }

                if(xmlPath.Equals("") || DSN.Equals("")){
                    view.ShowSettingsTab();
                    view.PrintInLogWindow("Configure el DSN y el XmlPath");
                }
                else
                {
                    

                    bool successDBConnection = InitDbConnection(DSN);
                    bool successInitializeWatcher = SetWatcherPath(xmlPath);

                    Thread.Sleep(1000);

                    try
                    {
                        websock = new Thread(new ThreadStart(InicializarWeb));
                        websock.Start();
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("No se pudo Conectar el WebSocket:" + e);
                        Console.WriteLine("Valide que el usuario sea correcto.");
                    }
                    view.PrintInLogWindow("Esperando que se facture");
                }
            }
        }

        private void ChiliLogin(string username, string password)
        {          
            view.PrintInLogWindow("Haciendo Login");
            view.PrintLoginNotification("Haciendo Login");
            string data = "";
            (successLogin, data) = PostCajeroLogin(username, password);
            if (successLogin == true)
            {
               // ValidateStatePayUser("a");
                tokenChili = data;
                view.PrintInLogWindow("Login exitoso.");
                view.PrintLoginNotification("Login exitoso.");

            }
            else
            {
                view.PrintInLogWindow(data);
                view.PrintLoginNotification(data);

            }
        }

        

        /*Metodo de identificacion de estado de pago del usuario*/
        private void ValidateStatePayUser(string username)
        {
            var client = new RestClient("http://chiliawsho.ddns.net:5000/api/user/StatePayUser");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", username, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine("Estado de pago:" + response.Content);
        }

        private void RemoveMessageFromTxtLoginNotification()
        {
            Thread.Sleep(6000);
            view.PrintLoginNotification("----");
        }

        public bool InitDbConnection(string DSN){
            /*
                * Se necesita la conexion con la bd para determinar
                * la sucursal a la que pertenece la estacion una vez
                * se hace el login
                * Ej: Al hacer el login nos devuelve un
                * json con mas o menos 4 id_sucursal, cada una con su nombre.
                * Lo que hacemos es ejecutar un query que nos trae
                * el id_sucursal de la bd, procedemos entonces a hacer el match
                * entre la sucursal del pos y el conjunto de sucursales 
                * que nos devuelve el API
                * 
                * 
                * Aqui valida que se tenga conexion a la bd y si no, entonces notifica al usuario que no se podrá
                * hacer nada y se queda en un while intentando cada 5 segs conectarse
                * hasta que haya conexion
                * */
            bool successConnection = false;
            while (!successConnection)
            {
                if (DSN.Equals(""))
                {
                    view.PrintInLogWindow("Configure el DSN");
                    System.Threading.Thread.Sleep(5000);                    
                }
                else
                {
                    try
                    {
                        successConnection = StartDBconnection(DSN);
                    }
                    catch (Exception e)
                    {
                        //Connection.stateConnection = "close";
                        exceptionsLog.SaveMessage(e.Message);
                    }
                    if(!successConnection){
                        view.PrintInLogWindow("Conexion con bd fallida. Verifique que haya iniciado el servicio de base de datos. Verifique el DSN que ingresó.");
                        view.PrintInLogWindow("En 5 segundos: nuevo intento de conexion con bd");
                        System.Threading.Thread.Sleep(5000);
                    }
                }
            }
            view.PrintInLogWindow("Conexion con bd exitosa");
            return successConnection;
        }

        /// Metodo que establece la conexion con la bd
        public bool StartDBconnection(string DSN)
        {
            connection = ConnectionSQLAnywhere.GetConnection(DSN);
            consultasDB = new ConsultasDB();
            consultasDB.SetDBconnection(connection.GetDBinstance());
            return ConnectionSQLAnywhere.successConnection;
        }

        public bool SetWatcherPath(string xmlPath){
            bool successWatcherPath = false;
            //Se ejecuta el watcher que estará pendiente de cuando se facture
            //Este llamará el método notify de esta clase WebService
            try
            {
                watcher.Attach(this, xmlPath);
                messagesLog.SaveMessage("Watcher escuchando.");
                view.HideWindow();
                successWatcherPath = true;

            }
            catch (Exception e)
            {
                messagesLog.SaveMessage("Watcher fallo. Verifique que ingreso bien la ruta donde se generan los xml del POS");
                exceptionsLog.SaveMessage(e.Message);
            }
            return successWatcherPath;
        }



        /*
        * Método que es llamado por el watcherXML para notificar que hubo una facturación del POS
        * Luego lo que se hace es llamar al método del watcher que entrega el id de la
        *  factura que se va a procesar para poder cargar los puntos
        * 
        * */

        public void Notify()
        {
            //lista de elementos a remover de la cola de idFacts
            List<int> elements2Remove = new List<int>();
            try
            {
                //Si no ha hecho el login lanza la excepcion
                if (successLogin != true)
                {
                    throw new Exception("Realice el inicio de sesión");
                }

                //* Verificar que la conexion a base de datos este OK
                if (!ConnectionSQLAnywhere.successConnection)
                {
                    connection.ResetConnection();
                    view.PrintInLogWindow("Se perdio la conexion con la base de datos. Intentando reestablecer");
                }

                string posIdInvoice = watcher.PopIdFactura();
                // Encolar:
                InvoiceSenderList.Add(posIdInvoice);

                // Recorrido de elementos en la cola (Incluye los que aun no han sido desencolados):
                for (int i = 0; i < InvoiceSenderList.Count; i++)
                {
                    bool successTransaction = false;
                    string idInvoice = InvoiceSenderList[i];
                    messagesLog.SaveMessage("Factura por procesar: " + idInvoice);
                    
                    successTransaction = UploadInvoice(idInvoice, tokenChili);
                    GetTokenMisFacturas();

                    /*Hilo de logica de mis facturas para que leal*/

                    /* 
                    (bool successPoints, string lealAuthToken, string idTransactUploadPoints, string uid_cms, string idSucursal) = UploadPoints(idInvoice, credenciales[0], EncryptPassword.ToString(), idSucursal_db, vlrFactura, idCard, tokenChili);
                    if (successPoints)
                    {
                        if (!idTransactUploadPoints.Equals(""))
                        {
                            bool isVoided = consultasDB.FiltroVoidedFact(idInvoice);
                            if (!isVoided)
                            {
                                string jsonFact = EstructurarJsonDetalleFactura(idTransactUploadPoints, idInvoice, uid_cms, idSucursal);
                                if (jsonFact != null)
                                {
                                    bool successPostDetalleFactura = PostDetalleFactura(jsonFact, lealAuthToken, tokenChili);
                                    successTransaction = successPostDetalleFactura;
                                }
                                else
                                {
                                    messagesLog.SaveMessage("No se pudo estructurar el json de detalleFactura");
                                }
                                
                            }
                            else
                            {
                                string message = "Es una anulación, no se carga detalle de factura";
                                messagesLog.SaveMessage(message);
                                view.PrintInLogWindow(message);
                                successTransaction = true;

                            }
                        }
                    }
                    */

                    if (successTransaction)
                    {
                        //Si termina correctamente ahí si lo añade a la lista de elementos a remover
                        elements2Remove.Add(i);
                        messagesLog.SaveMessage("Factura procesada sin errores: " + idInvoice);
                        view.PrintInLogWindow("Factura procesada sin errores: " + idInvoice);
                    }
                }

            }
            catch (NullReferenceException e)
            {
                view.PrintInLogWindow("No se pudo procesar la factura");
                messagesLog.SaveMessage("No se pudo procesar la factura");
                exceptionsLog.SaveMessage(e.ToString());
            }
            catch (WebException e)
            {
                exceptionsLog.SaveMessage("Excepcion web: " + e.ToString());
            }
            catch (InvalidOperationException sqlEx)
            {
                exceptionsLog.SaveMessage("Operacion invalida: " + sqlEx.Message);
            }
            catch (ArgumentOutOfRangeException outRangeEx)
            {
                exceptionsLog.SaveMessage("Salio de rango en la lista: " + outRangeEx.Message);
            }
            catch (System.ArgumentException argEx)
            {
                exceptionsLog.SaveMessage("Operacion invalida: " + argEx);
            }
            catch (Exception e)
            {
                exceptionsLog.SaveMessage("Excepcion: " + e.ToString());
                view.PrintInLogWindow("Error al procesar. Se encola la factura.");
            }
            finally
            {

                //Elimina los elementos tagueados desde la ultima posicion hasta la primera
                //para evitar un out of range
                elements2Remove.OrderByDescending(i => i).ToList().ForEach(pos => { InvoiceSenderList.RemoveAt(pos); });
                if (InvoiceSenderList.Count > 0)
                {
                    messagesLog.SaveMessage("Cola de facturacion:");
                    //InvoiceSenderList.ForEach(x => { view.PrintInConsole(x + ", "); });
                    //view.PrintInConsole("\n");
                    messagesLog.SaveMessage(string.Join(",", InvoiceSenderList));
                }
            }

        }

        /*Metodo de validacion de Mis facturas*/
        public bool GetTokenMisFacturas()
        {
            string userMisFacturas = "fullpruebas";
            string passMisFacturas = "123456a";
            var credenciales = new
            {
                username = userMisFacturas,
                password = passMisFacturas
            };
            string jsonFormat = JsonConvert.SerializeObject(credenciales);
            Console.WriteLine((string)jsonFormat);
            string url = "https://misfacturas.cenet.ws/IntegrationAPI/api/login/?"+"username="+ userMisFacturas + "&password="+ passMisFacturas;
            var client = new RestClient("http://localhost:5000/api/prueba/GetTokenMisFacturas");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            request.Timeout = 60000;
            Console.WriteLine("Token mis facturas" + response.Content + response.StatusCode);
            return true;
        }

        private string EncryptPasswordLeal(string Password)
        {
            string PassEncrypt = string.Empty;
            string hash = "c@h!l!Sy$t2m";
            byte[] data = UTF8Encoding.UTF8.GetBytes(Password);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            using(TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider(){Key = keys,Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7}){
                ICryptoTransform trasnsform = tripDes.CreateEncryptor();
                byte[] result = trasnsform.TransformFinalBlock(data, 0, data.Length);
                PassEncrypt = Convert.ToBase64String(result, 0, result.Length);
            } 
            //string EncryptP=Encoding.UTF8.GetString(Convert.FromBase64String(Password));
            // return Eramake.eCryptography.Encrypt(Password);
            messagesLog.SaveMessage("Encriptando en el metodo: "+PassEncrypt);
            return PassEncrypt.ToString();
        }


       private string EncryptUserAndPasswordLeal(string User,string Password)
        {
            string matchUserAndPass = User + ":" + Password;
            string PassEncrypt = string.Empty;
            string hash = "c@h!l!Sy$t2m";
            byte[] data = UTF8Encoding.UTF8.GetBytes(matchUserAndPass);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            {
                ICryptoTransform trasnsform = tripDes.CreateEncryptor();
                byte[] result = trasnsform.TransformFinalBlock(data, 0, data.Length);
                PassEncrypt = Convert.ToBase64String(result, 0, result.Length);
            }
            //string EncryptP=Encoding.UTF8.GetString(Convert.FromBase64String(Password));
            // return Eramake.eCryptography.Encrypt(Password);
            messagesLog.SaveMessage("Encriptando en el metodo: " + PassEncrypt);
            return PassEncrypt.ToString();
        }

        

     
        public (bool successLogin, string data) PostCajeroLogin(string username, string password)
        {
            bool successLogin = false;
            string data = "";
            //string username = "admin";
            //string password = "copete";
            //Console.WriteLine("Header:" + username + "" + password);
            messagesLog.SaveMessage("header:"+username + " "+ password);
            try
            {
                var client = new RestClient("http://chiliawsho.ddns.net:5000/api/user/login");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Cache-Control", "no-cache");
                //Codificamos el usuario y la contraseña
                string encoded = EncryptUserAndPasswordLeal(username, password);
                //view.PrintInLogWindow("Authorization: Basic " + encoded);
                request.AddHeader("Authorization", "Basic "+encoded);
                IRestResponse response = client.Execute(request);
                JObject respuesta = JObject.Parse(response.Content);
                //string JsonResponse = response.Content.ToString();
                if (respuesta["code"].ToString().Equals("400"))
                {
                    data = respuesta["tokenChili"].ToString();
                    messagesLog.SaveMessage("Token:" + data);
                    successLogin = true;
                    /*Inicializacion de socket*/
                    
                }
                else
                {
                    //metodo de escucha por servidor propio 
                    messagesLog.SaveMessage($"Error al hacer login a api propia. Infomessage: {respuesta["infomessage"].ToString()}; Errmessage {respuesta["errmessage"].ToString()}");
                    data = $"Fallo: {respuesta["infomessage"].ToString()} {respuesta["errmessage"].ToString()}";
                }
            }
            catch(Exception e)
            {                
                data = "Error del manager al hacer login.";
                messagesLog.SaveMessage("Excepcion Cajero:"+e);
            }
            
            return (successLogin, data);
        }

        public (bool successPoints, string lealAuthToken, string idTransactUploadPoints, string uid_cms, string idSucursal) UploadPoints(string posIdFact, string user, string password, string idSucursal, string valorFactura, string cedula, string tokenChili)
        {
            messagesLog.SaveMessage("passwordEncryptadoMD5:" + password + " " + "User:" + user);
            bool successTransaction = false;
            string lealAuthToken = "";
            string idTransactUploadPoints = "";
            string uid_cms = "";
            string id_sucursal = "";
            /*"valorFactura":"2000",
            "posIdFactura":"1002",
            "posIdSucursal":"908",
            "username":"cajero",
            "password":"******",
            "idCard":"1017260121"*/
            var jsonData = new
            {
                valorFactura = valorFactura,
                posIdFactura = posIdFact,
                posIdSucursal = idSucursal,
                username = user,
                password = password,
                idCard = cedula,
            };
            
            string jsonFormat = JsonConvert.SerializeObject(jsonData);
            view.PrintInLogWindow("Cargando puntos");//: " + jsonFormat);
            var client = new RestClient("http://chiliawsho.ddns.net:5000/Service/Leal/uploadpoints");
            var request = new RestRequest(Method.POST);

            request.Timeout = 60000;
            request.AddHeader("Authorization", "Bearer "+tokenChili);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
            messagesLog.SaveMessage("json: " + jsonFormat);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString().Equals("OK"))
            {
                messagesLog.SaveMessage("Respuesta servidor propio:" + response.Content.ToString());
                var contentResponse = response.Content;
                string sanitizedJsonResponse = SanitizeResponse((string)contentResponse);
                JObject ObjectJson = GetDeserializedJSON(sanitizedJsonResponse);
                bool successCargaPuntos = (bool)ObjectJson["success"];
                string infomessage = (string)ObjectJson["infomessage"];
                //Console.WriteLine("sanitizedJsonResponse");
                //Console.WriteLine(sanitizedJsonResponse);

                if (successCargaPuntos)
                {

                    lealAuthToken = (string)((ObjectJson["data"])["authTokenLeal"]);
                    idTransactUploadPoints = (string)((ObjectJson["data"])["idUploadPoints"]);
                    uid_cms = (string)((ObjectJson["data"])["uid_cms"]);
                    id_sucursal = (string)((ObjectJson["data"])["idSucursal"]);
                    view.PrintInLogWindow(infomessage);
                    successTransaction = true;
                }
                else
                {
                    string errmessage = (string)ObjectJson["errmessage"];
                    view.PrintInLogWindow(infomessage);
                    view.PrintInLogWindow(errmessage);
                }
            }
            else
            {
                string errmessage = $"Respuesta: {response.StatusCode.ToString()}";
                view.PrintInLogWindow(errmessage);
                messagesLog.SaveMessage($"Hubo un error al cargar puntos: {errmessage}");
            }
            
            return (successTransaction, lealAuthToken, idTransactUploadPoints, uid_cms, id_sucursal);
        }

        public bool UploadInvoice(string posIdInvoice, string tokenChili)
        {
            bool successTransaction = false;

            var jsonData = BuildJsonInvoiceDetails(posIdInvoice);
            string jsonFormat = JsonConvert.SerializeObject(jsonData);
            Console.WriteLine((string)jsonFormat);
            //string jsonFormat = JsonConvert.SerializeObject(jsonData);
            var client = new RestClient($"{webServiceHost}/service/uploadinvoice");
            var request = new RestRequest(Method.POST);
            Console.WriteLine("Estructura del json:"+jsonFormat);
            messagesLog.SaveMessage("Estrauctura captada en UploadInvoice:" + jsonFormat);
            request.Timeout = 60000;
            request.AddHeader("Authorization", "Bearer "+tokenChili);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
            //messagesLog.SaveMessage("json: " + jsonFormat);
            IRestResponse response = client.Execute(request);
            messagesLog.SaveMessage("Valor respuesta:" + response);
            if (response.StatusCode.ToString().Equals("OK"))
            {
                Console.WriteLine("Recibio respuesta de la carga de factura");
                var contentResponse = response.Content;
                string sanitizedJsonResponse = SanitizeResponse((string)contentResponse);
                JObject ObjectJson = GetDeserializedJSON(sanitizedJsonResponse);
                successTransaction = (bool)ObjectJson["success"];
                string infomessage = (string)ObjectJson["infomessage"];
                string errmessage = (string)ObjectJson["errmessage"];
                
                if (!infomessage.Equals(""))
                {
                    view.PrintInLogWindow(infomessage);
                }

                if (!errmessage.Equals(""))
                {
                    view.PrintInLogWindow(errmessage);
                }

            }
            else
            {
                string errmessage = $"Hubo un error al subir factura. Respuesta: {response.StatusCode.ToString()}";
                view.PrintInLogWindow(errmessage);
                messagesLog.SaveMessage(errmessage);
            }
            
            return successTransaction;
        }

        public object BuildJsonInvoiceDetails(string posIdInvoice)
        {
            try
            {
                //validacion de la factura antes realizar la estructura
                messagesLog.SaveMessage("IdTrans:" + posIdInvoice);
                bool isVoided = consultasDB.FiltroVoidedFact(posIdInvoice);
                messagesLog.SaveMessage($"La factura {posIdInvoice} fue anulada");
                string idCard = GetPOSClientIdCardByIdInvoice(posIdInvoice); //#by idFact
                Console.WriteLine($"el idcard {idCard} consultado");
                List<string> credenciales = consultasDB.GetEmpLealUserPw_by_factId(posIdInvoice); //[usr, pw]
                string idSucursal_db = consultasDB.GetNroSucursal();
                string posInvoiceAmount = consultasDB.GetValorFactById(posIdInvoice); // #by idFact

                view.PrintInLogWindow("Factura por procesar: " + posIdInvoice + " idsuc: " + idSucursal_db + " vlrFact: " + posInvoiceAmount + " idCard: " + idCard);
                string lealPassword = ""; 
                if (!credenciales[1].Equals(""))
                {
                    lealPassword = EncryptPasswordLeal(credenciales[1]);
                }
                string lealUsername = credenciales[0];
                
                Console.WriteLine("lealUsername:" + lealUsername + "PasswordEncriptadoMD5:"+lealPassword);

                List<Int64> arrItemsImpUnid = consultasDB.GetImpuestoUndByFactId(posIdInvoice);
                List<string> arrTipoImpuesto = consultasDB.GetTipoImpuestoByFactId(posIdInvoice);
                string formaPago = consultasDB.GetFormaPagoByFactId(posIdInvoice);
                var numValue=Convert.ToInt64(Convert.ToDouble(idSucursal_db.ToString()));
                Console.WriteLine("Valor de converison:" + numValue);

                bool parsed = Int64.TryParse(idSucursal_db, out Int64 idSucursal);
                if (!parsed)
                {
                    messagesLog.SaveMessage("uint.TryParse could not parse '" + idSucursal_db + "' to an uint.");
                    // Output: int.TryParse could not parse 'abc' to an int.
                    throw new Exception("uint.TryParse could not parse string to an uint.");
                }

                //-------------------------------------------------------------------
                // Hasta este punto tenemos los querys pequeños

                List<List<string>> itemsMatrix = consultasDB.GetItemsByFactId(posIdInvoice, arrItemsImpUnid, arrTipoImpuesto);

                //Hasta esta parte ya tenemos un arreglo con toda la parte del item

                messagesLog.SaveMessage("Matriz de items que son insertados:");
                foreach (var item in itemsMatrix)
                {
                    messagesLog.SaveMessage("Item: " + string.Join(",", item));
                }

                PropertyInfo[] classItemProperties = typeof(Item).GetProperties();
                List<Item> collectionItems = new List<Item>();
                foreach (List<string> subList in itemsMatrix)
                {

                    Item item = new Item();
                    messagesLog.SaveMessage("Detalles de Factura:");
                    item = (Item)FillClassFields(classItemProperties, item, subList);
                    //view.PrintInConsole("\n");
                    collectionItems.Add(item);
                }

                //aqui ya tenemos una lista de tipo Item para poder crear el json

                List<string> collectionDetailedItems = new List<string>();

                //Aqui ya tenemos los detailedItems

                List<string> factHeader = consultasDB.GetFactHeader(posIdInvoice);



                InvoiceDetail encabezadoFact = new InvoiceDetail(collectionItems, collectionDetailedItems);
                PropertyInfo[] propertiesFactura = typeof(InvoiceDetail).GetProperties();

                if (factHeader.Count > 0)
                {
                    encabezadoFact = (InvoiceDetail)FillClassFields(propertiesFactura, encabezadoFact, factHeader);
                    encabezadoFact.formaPago = formaPago.Substring(0, formaPago.Length - 1);
                    /*
                    "posInvoiceAmount":"2000",
                    "posIdFactura":"1002",
                    "posIdSucursal":"908",
                    "username":"cajero",
                    "password":"******",
                    "idCard":"1017260121"
                    */
                    string strencabezadoFact = JsonConvert.SerializeObject(encabezadoFact);
                    
                    var collectionWrapper = new
                    {
                        posIdInvoice = posIdInvoice,
                        factura = encabezadoFact,
                        idCard = idCard,
                        posInvoiceAmount = posInvoiceAmount,
                        posIdSucursal = idSucursal,
                        lealUsername = lealUsername,
                        lealPassword = lealPassword,
                        IsVoided=isVoided
                    };
                 
                    return collectionWrapper;
                }
                else
                {
                    messagesLog.SaveMessage("Fact_header esta vacío");
                }
            }catch(Exception e)
            {
                exceptionsLog.SaveMessage(e.ToString());
            }
            return null;

        }

        private Object FillClassFields(PropertyInfo[] properties, Object obj, List<string> list)
        {
           /* Console.WriteLine("INGRESE CON COUNTER EN LA LISTA>>>>>>>>>>>>>>>>>>>>>>><" + list.Count.ToString());
            for (int contador=0; contador<list.Count;contador++)
            {
                Console.WriteLine("FOR DE VALIDACION DE ELEMENTOS EN LAS POSICIONES------------->" + list[contador].ToString());
            }*/
            
            for (int counter = 0; counter < list.Count; counter++)
            {
                Console.WriteLine("Elemento en la POSICION COUNTER------------->" + list[counter].ToString());
                if (properties[counter].PropertyType == typeof(string))
                {
                    properties[counter].SetValue(obj, list[counter].ToString());
                }
                else if (properties[counter].PropertyType == typeof(int))
                {
                    bool parsed = int.TryParse(list[counter].ToString(), out int numValue);
                    if (!parsed)
                    {
                        messagesLog.SaveMessage("int.TryParse could not parse '" + list[counter].ToString() + "'to an int.");
                        // Output: int.TryParse could not parse 'abc' to an int.
                        throw new Exception("int.TryParse could not parse string to an int.");
                    }
                    else
                    {
                        properties[counter].SetValue(obj, numValue);
                        //messagesLog.SaveMessage("int: " + numValue.ToString());
                    }
                }
                else if (properties[counter].PropertyType == typeof(double))
                {
                    //Different cultures use different decimal separators (namely , and .).
                    //You can parse your doubles using overloaded method which takes culture as a second parameter. In this case you can use InvariantCulture
                    //You should also take a look at double.TryParse, you can use it with many options and it is especially useful to check wheter or not your string is a valid double.
                    //Imprimir el doble para ver si viene con ',' o con '.'
                    bool parsed = Double.TryParse(list[counter].ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out double numValue);
                    if (!parsed)
                    {
                        messagesLog.SaveMessage("double.TryParse could not parse '" + list[counter].ToString() + "'to an int.");
                        throw new Exception("double.TryParse could not parse string to an double.");
                    }
                    else
                    {
                        properties[counter].SetValue(obj, numValue);
                        //messagesLog.SaveMessage("doble: " + numValue.ToString());
                    }
                }
            }
            return obj;
        }


        private bool PostDetalleFactura(string jsonFact, string lealAuthToken, string tokenChili)
        {
            view.PrintInLogWindow("Cargando detalle de factura a Leal");
            messagesLog.SaveMessage("Cargando detalle de factura a Leal");
            messagesLog.SaveMessage("jsonFact:\n" + jsonFact);
            
            var jsonData = new
            {
                jsonDetalleFactura = jsonFact,
                lealAuthToken = lealAuthToken
            };
            string jsonFormat = JsonConvert.SerializeObject(jsonData);
            bool success = false;
            //view.PrintInLogWindow(jsonFact);
            var client = new RestClient("http://chiliawsho.ddns.net:5000/Service/Leal/detallefactura");
            var request = new RestRequest(Method.POST);
            request.Timeout = 60000;
            request.AddHeader("Authorization", "Bearer "+tokenChili);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);


            //Aqui pregunta por el status OK, sino notifica el login fallido
            if (response.StatusCode.ToString().Equals("OK"))
            {
                var content = response.Content;
                messagesLog.SaveMessage("postdetalle factura:");
                string sanitizedJson = SanitizeResponse((string)content);
                //view.PrintInLogWindow(sanitizedJson);
                
                JObject jsonResponse = GetDeserializedJSON(sanitizedJson);
                bool successPostDtlleFact = (bool)jsonResponse["success"];
                if (successPostDtlleFact)
                {
                    view.PrintInLogWindow("Detalle de factura fue cargado a Leal");
                    success = true;
                }
                else
                {
                    view.PrintInLogWindow("El servidor retorno un codigo de error al enviar detalle de factura");
                    messagesLog.SaveMessage("El servidor retorno un codigo de error al enviar detalle de factura");
                    //messagesLog.SaveMessage("code:" + code);
                }
            }
            else
            {
                //Notifica que no hubo respuesta del servidor
                messagesLog.SaveMessage("statusCode: " + response.StatusCode);
                view.PrintInLogWindow("No hubo respuesta del servidor");
            }
            return success;
        }

        /*
        public DataSet Resultados()
        {
            SADataAdapter consulta = new SADataAdapter("SELECT * FROM dba.posdetail", con);
            DataSet ds = new DataSet();
            consulta.Fill(ds);
            return ds;
        }

        //Obtenemos todos los empleados en formato json
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void ResultadoJSON()
        {
            SADataAdapter c = new SADataAdapter("Select * from dba.employee", con);
            DataSet en = new DataSet();
            c.Fill(en);
            Context.Response.Write(JsonConvert.SerializeObject(en, Formatting.Indented));
            //var cond = en;
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //return jss;

        }*/


        //Tomamos el json response y lo deserializamos para poderlo interpretar
        public JObject GetDeserializedJSON(string json)
        {
            //view.PrintInConsole("content: ");
            //messagesLog.SaveMessage(json);
            JObject jObject;
            try
            {
                messagesLog.SaveMessage("JSON Factura:"+json);
                
                jObject = JObject.Parse(json);
                messagesLog.SaveMessage("JObjetc:" + jObject.ToString());
            }
            catch (Newtonsoft.Json.JsonReaderException jsonEx)
            {
                messagesLog.SaveMessage("Excepcion json: " + jsonEx.ToString());
                return null;
            }
            return jObject;
        }

        private string SanitizeResponse(string json)
        {
            string sanitized = json.Trim(new char[] { '\uFEFF', '\uFFFE', '\u200B', });
            return sanitized;
        }

        public string GetPOSClientIdCardByIdInvoice(string posIdFact)
        {
            string cedula = "";
            string memcode = consultasDB.GetMemCodeByFactId(posIdFact);
            if (!memcode.Equals("0"))
            {
                cedula = consultasDB.GetCedulaByMemcode(memcode);
            }
            return cedula.Trim();
        }

        public string GetEstadoPagoWebApi()
        {
            //string idsucursal = "1";
            string ResponseServer;
            WebRequest request = WebRequest.Create("http://localhost:51289/api/values/{idsucursal}");
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";
            request.Timeout = 60000;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            /*Con el stream obtenemos el token que nos da el servidor almacenadndo en un string llamado responseServer*/
            Stream DatosServidosToken = response.GetResponseStream();
            StreamReader reader = new StreamReader(DatosServidosToken);
            ResponseServer = reader.ReadToEnd();
            DatosServidosToken.Close();
            reader.Close();
            return ResponseServer;
        }

        public void NotifyError(string message, Exception e)
        {
            exceptionsLog.SaveMessage(message + ": " + e);
            view.PrintInLogWindow(message);
        }

        private void CancelKeyPressed(object sender, ConsoleCancelEventArgs args)
        {
            SystemExit();
        }

        private void SystemExit()
        {
            view.ShowBalloonNotification("Saliendo del sistema");
            view.KillGUI();
            Environment.Exit(1);
        }
        static void Main(string[] args) { }
    }
}