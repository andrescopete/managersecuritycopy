﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using iAnywhere.Data.SQLAnywhere;



namespace ModelLayer
{
    public class ConsultasDB
    {
        SAConnection conexionDB;

        public ConsultasDB()
        {
        }

        public void SetDBconnection(SAConnection connection)
        {
            conexionDB = connection;
        }
        public DataSet Getdatos()
        {
            SADataAdapter consulta = new SADataAdapter();
            consulta.SelectCommand = new SACommand("Select * from dba.employee", conexionDB);
            DataSet ds = new DataSet();
            consulta.Fill(ds);
            return ds;
        }


        /*
         * Método que ejecuta un query sobre la base de datos
         * y retorna el primer elemento de la primera fila
         * 
         * */
        private string ExecuteQuery(string query)
        {
            string id = "";
            if (ConnectionSQLAnywhere.successConnection == true)
            {
                try
                {

                    SACommand comando = new SACommand(query, conexionDB);
                    SADataReader reader = comando.ExecuteReader();

                    if (reader.Read())
                    {
                        id = SafeGetString(reader, 0);
                    }
                    reader.Close();
                }
                catch (SAException e)
                {
                    throw new Exception(e.ToString());
                }
            }
            return id;
        }

        public string SafeGetString(SADataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return null;
        }
        /**
         * Obtiene el elemento de la cola y consulta en la base de datos.
         * Si esa factura no tiene miembro asociado, devuelve null
         * 
         * */
        public bool FiltroHasMemcode(string posIdFactFiltro)
        {
            if (posIdFactFiltro == null)
                return false;
            string queryFiltro = "Select memcode from dba.posheader where transact =" + posIdFactFiltro;
            string memcode = ExecuteQuery(queryFiltro);
            if (memcode.Equals("0"))
            {
                return false;
            }
            return true;
        }

        public bool FiltroVoidedFact(string posIdFactFiltro)
        {
            bool isVoided = false;
            if (posIdFactFiltro == null)
                return false;
            string queryFiltro = "select voided from dba.howpaid where transact =" + posIdFactFiltro;
            SACommand comando = new SACommand(queryFiltro, conexionDB);
            SADataReader readerVoided = comando.ExecuteReader();

            while (readerVoided.Read())
            {
                Console.WriteLine("Valor optenido de la lectura:"+readerVoided.GetValue(0).ToString());
                if (readerVoided.GetValue(0).ToString().Equals("1"))
                {
                    isVoided = true;
                }
                else
                {
                    Console.WriteLine("entre al else");
                    isVoided = false;
                }
            }
            readerVoided.Close();

            return isVoided;
        }

        /*Validar nombre de la sucursal de Pixel Pos*/
        public string GetNombreSucursal()
        {
            string nameSucursalPos=string.Empty;
            string queryFiltro = "select contact from dba.sysinfo  ";
            SACommand comando = new SACommand(queryFiltro, conexionDB);
            SADataReader readerVoided = comando.ExecuteReader();
            while (readerVoided.Read())
            {
                //Console.WriteLine("Valor encontrado" + readerVoided.GetValue(0).ToString());
                nameSucursalPos = readerVoided.GetValue(0).ToString();
            }
            return nameSucursalPos;
        }

        public string GetNroSucursal()
        {
            string querySucursal = "SELECT prov FROM dba.sysinfo";
            string id_sucursal_db = ExecuteQuery(querySucursal);
            return id_sucursal_db;
        }

        public string GetValorFactById(string idFactura)
        {
            string Querymemcode = "select NETTOTAL from dba.posheader where transact =" + idFactura;
            string vlrFactura = ExecuteQuery(Querymemcode);
            return vlrFactura;
        }

        public string GetMemCodeByFactId(string posIdFact)
        {
            string Querymemcode = "select memcode from dba.posheader where transact =" + posIdFact;
            string memcode = ExecuteQuery(Querymemcode);
            return memcode;
        }

        public string GetCedulaByMemcode(string memcode)
        {
            string Querycedula = "select cellnum from dba.member where memcode =" + memcode;
            string cedula = ExecuteQuery(Querycedula);
            return cedula;
        }

        public List<string> GetEmpLealUserPw_by_factId(string factId)
        {
            string userPos = "", passPos = "";

            if (factId != null)
            {
                string queryUser = "select df.EmpUserDefID, df.Value from dba.POSHEADER ph inner join dba.EmpUserDefFlds df on df.EmpNum = ph.WHOCLOSE inner join dba.UserDefFlds udf on df.UserDefID = udf.UserDefID where ph.TRANSACT = " + factId + "and udf.Name = 'Leal Usuario'";
                string queryPass = "select df.EmpUserDefID, df.Value from dba.POSHEADER ph inner join dba.EmpUserDefFlds df on df.EmpNum = ph.WHOCLOSE inner join dba.UserDefFlds udf on df.UserDefID = udf.UserDefID where ph.TRANSACT = " + factId + " and udf.Name = 'Leal PWD'";

                SACommand comando = new SACommand(queryUser, conexionDB);
                SADataReader readerDataLoguin = comando.ExecuteReader();
                SACommand comando2 = new SACommand(queryPass, conexionDB);
                SADataReader readerDataLoguin2 = comando2.ExecuteReader();

                if (readerDataLoguin.Read() && readerDataLoguin2.Read())
                {
                    userPos = readerDataLoguin.GetValue(1).ToString();
                    passPos = readerDataLoguin2.GetValue(1).ToString();
                }
                readerDataLoguin.Close();
                readerDataLoguin2.Close();
            }
            return new List<string> { userPos, passPos };
        }

        public List<Int64> GetImpuestoUndByFactId(string idFacturaPOS)
        {
            List<Int64> arrItemsImpUnid = new List<long>();
            //string queryImpuestoUnidad = "select case when pd.ApplyTax1 = 1 then(pd.costeach * COALESCE((sis.TAXRATE1 / 100),0)) else 0 END as tax1,case when pd.ApplyTax2 = 1 then(pd.costeach * COALESCE((sis.TAXRATE2 / 100),0)) else 0 END as tax2,case when pd.ApplyTax3 = 1 then(pd.costeach * COALESCE((sis.TAXRATE3 / 100),0)) else 0 END as tax3,case when pd.ApplyTax4 = 1 then(pd.QUAN * COALESCE((sis.TAXRATE4 / 100),0)) else 0 END as tax4,case when pd.ApplyTax5 = 1 then 0 else 0 END as tax5 from dba.Sysinfo sis, dba.POSDETAIL pd inner join dba.product pr on pd.PRODNUM = pr.PRODNUM inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact = " + idFacturaPOS + " AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
            string queryImpuestoUnidad = "select case when pd.ApplyTax1 = 1 then(pd.costeach * (coalesce((sis.TAXRATE1 / 100), 0))) else 0 END as tax1,case when pd.ApplyTax2 = 1 then(pd.costeach * (coalesce((sis.TAXRATE2 / 100), 0))) else 0 END as tax2,case when pd.ApplyTax3 = 1 then(pd.costeach * (coalesce((sis.TAXRATE3 / 100), 0))) else 0 END as tax3,case when pd.ApplyTax4 = 1 then(pd.QUAN * (coalesce((sis.TAXRATE4 / 100), 0))) else 0 END as tax4,case when pd.ApplyTax5 = 1 then 0 else 0 END as tax5 from dba.Sysinfo sis, dba.POSDETAIL pd inner join dba.product pr on pd.PRODNUM = pr.PRODNUM inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact = " + idFacturaPOS + "AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
            SACommand comando = new SACommand(queryImpuestoUnidad, conexionDB);
            SADataReader readerImpUnidad = comando.ExecuteReader();
            while (readerImpUnidad.Read())
            {
                Int64 impUnid = 0;
                
                for (int i = 0; i < readerImpUnidad.FieldCount; i++)
                {
                    
                     Console.WriteLine("SI TIENE IMPUESTOS"+readerImpUnidad.GetValue(i) + readerImpUnidad.GetValue(i).ToString());
                     var numValue=Convert.ToInt64(Convert.ToDouble(readerImpUnidad.GetValue(i).ToString()));
                     Console.WriteLine(readerImpUnidad.GetValue(i)+ ""+":"+numValue+readerImpUnidad.GetValue(i).ToString().All(Char.IsDigit));
                  //  bool parsed = Int64.TryParse(readerImpUnidad.GetValue(i).ToString(), out Int64 numValue);

                    //if (!parsed)
                    //{
                        // Output: int.TryParse could not parse 'abc' to an int.
                      //  throw new Exception("int.TryParse could not parse string to an int.");
                    //}
                    //else
                    //{
                        impUnid += numValue;
                        //}
                    
                }
                arrItemsImpUnid.Add(impUnid);
            }
            readerImpUnidad.Close();
            return arrItemsImpUnid;
        }

        public List<List<string>> GetItemsByFactId(string idFacturaPOS, List<Int64> arrItemsImpUnid, List<string> arrTipoImpuesto)
        {
            Console.WriteLine("VALOR DE ELEMENTOS EN LA LISTA:" + "ARRITEMIMPUESTOUNIDAD" + arrItemsImpUnid.Count.ToString() + "TIPO DE IMPUESTO" + arrTipoImpuesto.Count.ToString());
            for (int c=0; c<arrItemsImpUnid.Count;c++)
            {
                Console.WriteLine("DESDE EL METODO GETITEMSBYFACTID<><><>-->" + arrItemsImpUnid[c].ToString());

            }

            for (int c = 0; c < arrTipoImpuesto.Count; c++)
            {
                Console.WriteLine("DESDE EL METODO ARRTIPOIMPUESTO<><><>-->" + arrTipoImpuesto[c].ToString());

            }
            List<List<string>> itemsMatrix = new List<List<string>>();
            //string queryItems = "select pd.recpos, pd.prodnum, pr.descript, case when pd.linedes is null then '0' else  pd.linedes END as Linesdesc, pd.quan, pd.costeach, pd.costeach + case when pd.ApplyTax1 = 1 then (pd.costeach * (sis.TAXRATE1 / 100)) else 0 END + case when pd.ApplyTax2 = 1 then(pd.costeach * (sis.TAXRATE2 / 100)) else 0 END + case when pd.ApplyTax3 = 1 then(pd.costeach * (sis.TAXRATE3 / 100)) else 0 END + case when pd.ApplyTax4 = 1 then(pd.QUAN * (sis.TAXRATE4 / 100)) else 0 END + case when pd.ApplyTax5 = 1 then 0 else 0 END as precioTotal from dba.Sysinfo sis, dba.POSDETAIL pd inner join dba.product pr on pd.PRODNUM = pr.PRODNUM inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact = " + idFacturaPOS + " AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
            string queryItems = "select pd.recpos, pd.prodnum, pr.descript, coalesce(pd.linedes, 0) as Linesdesc, pd.quan, pd.costeach, pd.costeach + case when pd.ApplyTax1 = 1 then(pd.costeach * (coalesce((sis.TAXRATE1 / 100), 0))) else 0 END + case when pd.ApplyTax2 = 1 then(pd.costeach * (coalesce((sis.TAXRATE2 / 100), 0))) else 0 END + case when pd.ApplyTax3 = 1 then(pd.costeach * (coalesce((sis.TAXRATE3 / 100), 0))) else 0 END + case when pd.ApplyTax4 = 1 then(pd.QUAN * (coalesce((sis.TAXRATE4 / 100), 0))) else 0 END + case when pd.ApplyTax5 = 1 then 0 else 0 END as precioTotal from dba.Sysinfo sis, dba.POSDETAIL pd inner join dba.product pr on pd.PRODNUM = pr.PRODNUM inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact = " + idFacturaPOS + " AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
                 SACommand comando = new SACommand(queryItems, conexionDB);
            SADataReader readerItems = comando.ExecuteReader();

            int contItems = 0;
            while (readerItems.Read())
            {
                List<string> sublist = new List<string>();
                for (int i = 0; i < readerItems.FieldCount; i++)
                {
                    Console.WriteLine(String.Format("{0} = {1}", "col", readerItems.GetValue(i)));
                    sublist.Add(readerItems.GetValue(i).ToString());

                }
                sublist.Add(arrItemsImpUnid[contItems].ToString());
                sublist.Add(arrTipoImpuesto[contItems].Substring(0, arrTipoImpuesto[contItems].Length - 1));
                itemsMatrix.Add(sublist);
                contItems++;
            }
            readerItems.Close();
            return itemsMatrix;
        }

        public List<string> GetFactHeader(string idFacturaPOS)
        {
            List<string> factHeader = new List<string>();
            string queryFactHeader = "select ph.NUMCUST as totalPersonas, (convert(varchar, sis.prov)+'-'+convert(varchar, ph.TRANSACT)) as 'clave' ,ph.TRANSACT as 'noFactura', convert( varchar, ph.TIMEEND, 120) as 'fecha', convert( varchar, ph.OPENDATE , 120) as 'fechaApertura', convert( varchar, ph.OPENDATE , 120) as 'fechaCierre',ph.WHOCLOSE,ph.NETTOTAL,ph.GratAmount,ph.TAX1 + ph.TAX2 + ph.TAX3 + ph.TAX4 + ph.TAX5 as 'impuestoTotal' from dba.Sysinfo sis, dba.POSHEADER ph where ph.transact = " + idFacturaPOS;
            SACommand comando = new SACommand(queryFactHeader, conexionDB);
            SADataReader readerEncabezadoFact = comando.ExecuteReader();
            if (readerEncabezadoFact.Read())
            {
                for (int counterFactHeader = 0; counterFactHeader < readerEncabezadoFact.FieldCount; counterFactHeader++)
                {
                    factHeader.Add(readerEncabezadoFact.GetValue(counterFactHeader).ToString());
                }
            }
            readerEncabezadoFact.Close();
            return factHeader;
        }

        public string GetFormaPagoByFactId(string idFacturaPOS)
        {
            string formaPago = "";
            string queryMetodoPago = "select mp.descript from dba.MethodPay mp inner join dba.Howpaid hp on hp.METHODNUM = mp.METHODNUM where hp.TRANSACT = " + idFacturaPOS + " AND hp.VoidedLink = 0";
            SACommand comando = new SACommand(queryMetodoPago, conexionDB);
            SADataReader readerPagos = comando.ExecuteReader();
            while (readerPagos.Read())
            {
                formaPago += readerPagos.GetValue(0).ToString() + ",";
            }
            //Debug.WriteLine(String.Format("{0}:{1}", "forma Pago", formaPago));
            readerPagos.Close();
            return formaPago;
        }

        public List<string> GetTipoImpuestoByFactId(string idFacturaPOS)
        {
            List<string> arrTipoImpuesto = new List<string>();
            //string queryTaxDesc = "select case when pd.ApplyTax1 = 1 then sis.TAXDES1 else '0' END as impuesto1,case when pd.ApplyTax2 = 1 then sis.TAXDES2 else '0' END as impuesto2,case when pd.ApplyTax3 = 1 then sis.TAXDES3 else '0' END as impuesto3,case when pd.ApplyTax4 = 1 then sis.TAXDES4 else '0' END as impuesto4,case when pd.ApplyTax5 = 1 then sis.TAXDES5 else '0' END as impuesto5 from dba.Sysinfo sis, dba.POSDETAIL pd  inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact = " + idFacturaPOS + " AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
            string queryTaxDesc = "select case when pd.ApplyTax1 = 1 then coalesce(sis.TAXDES1,0) else '0' END as impuesto1,case when pd.ApplyTax2 = 1 then coalesce(sis.TAXDES2,0) else '0' END as impuesto2,case when pd.ApplyTax3 = 1 then coalesce(sis.TAXDES3,0) else '0' END as impuesto3,case when pd.ApplyTax4 = 1 then coalesce(sis.TAXDES4,0) else '0' END as impuesto4,case when pd.ApplyTax5 = 1 then coalesce(sis.TAXDES5,0) else '0' END as impuesto5 from dba.Sysinfo sis, dba.POSDETAIL pd  inner join dba.POSHEADER ph on pd.TRANSACT = ph.TRANSACT where ph.transact =" + idFacturaPOS + " AND pd.PRODTYPE < 100 AND pd.COSTEACH > 0 order by pd.RECPOS";
             SACommand comando = new SACommand(queryTaxDesc, conexionDB);
            SADataReader readerTaxDesc = comando.ExecuteReader();
            while (readerTaxDesc.Read())
            {
                string impDesc = "";
                for (int i = 0; i < readerTaxDesc.FieldCount; i++)
                {
                    Console.WriteLine("GetTipoImpuestoByFactura"+readerTaxDesc.GetValue(i).ToString());
                    //Debug.WriteLine(String.Format("{0} = {1}", "col", reader.GetValue(i)));
                    string tax = readerTaxDesc.GetValue(i).ToString();
                    if (!tax.Equals("0"))
                    {
                        impDesc += readerTaxDesc.GetValue(i).ToString() + ",";
                    }
                }
                arrTipoImpuesto.Add(impDesc);
            }
            readerTaxDesc.Close();
            return arrTipoImpuesto;
        }
    }
}
